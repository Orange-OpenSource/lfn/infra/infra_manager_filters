#!/usr/bin/env python3

import interfaces
import pytest

"""
    Test nodes module
"""

target_interfaces = interfaces.FilterModule().filters()['target_interfaces']
mac2intf = interfaces.FilterModule().filters()['mac2intf']


def test_target_interfaces():
    nodes = {'foo': {'interfaces': [{'mac_address': 'aa:bb:cc:dd:ee:f1'},
                                    {'mac_address': 'aa:bb:cc:dd:ee:f2'}]}}
    hostvars = {'bar': {'ansible_hostname': 'foo'}}
    assert target_interfaces('bar', hostvars, nodes) == ['aa:bb:cc:dd:ee:f1',
                                                         'aa:bb:cc:dd:ee:f2']


def test_mac2intf():

    nodes = {'foo': {'interfaces': [{'mac_address': 'aa:bb:cc:dd:ee:f1'},
                                    {'mac_address': 'aa:bb:cc:dd:ee:f2'},
                                    {'mac_address': 'aa:bb:cc:dd:ee:f3'}]}}
    hostvars = {'bar': {'ansible_hostname': 'foo',
                        'ansible_interfaces': ['eth0',
                                               'eth1',
                                               'eth1.123'],
                        'ansible_eth0': {'macaddress': 'aa:bb:cc:dd:ee:f1'},
                        'ansible_eth1': {'macaddress': 'aa:bb:cc:dd:ee:f2'},
                        'ansible_eth1.123': {'macaddress': 'aa:bb:cc:dd:ee:f2'}
                        }
                }
    assert mac2intf('bar', hostvars, nodes) == {'aa:bb:cc:dd:ee:f1': 'eth0',
                                                'aa:bb:cc:dd:ee:f2': 'eth1'}
