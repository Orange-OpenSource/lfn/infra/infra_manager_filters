#!/usr/bin/env python3

import nodes
import pytest
from ansible import errors


"""
    Test nodes module
"""

nodes_as_dict = nodes.FilterModule().filters()['nodes_as_dict']
nodes_index = nodes.FilterModule().filters()['nodes_index']
nodes_filter = nodes.FilterModule().filters()['nodes_filter']
role2nodes = nodes.FilterModule().filters()['role2nodes']
nodes_name = nodes.FilterModule().filters()['nodes_name']
nodes_net_config = nodes.FilterModule().filters()['nodes_net_config']


def test_nodes_as_dict():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []}]
    assert nodes_as_dict(nodes) == {'foo1': {'name': 'foo1', 'a': []},
                                    'foo2': {'name': 'foo2', 'a': []}}


def test_nodes_index_no_jumphost0():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []}]
    assert nodes_index(nodes) == {'foo1': 1, 'foo2': 2}


def test_nodes_index_with_jumphost0():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []},
             {'name': 'jumphost0', 'a': []}]
    assert nodes_index(nodes) == {'foo1': 1, 'foo2': 2, 'jumphost0': 0}


def test_nodes_index_with_zero_key():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []},
             {'name': 'jumphost0', 'a': []}]
    assert nodes_index(nodes, 'foo2') == {'foo1': 1, 'foo2': 0, 'jumphost0': 2}


def test_nodes_filter():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []},
             {'name': 'foo3', 'a': []}]
    nodes_roles = {'foo1': ['f1', 'f2'], 'foo2': ['f2'], 'foo3': ['f3']}
    deploy_def = {'f1': 1, 'f2': 2, 'f3': 0}
    assert nodes_filter(nodes, nodes_roles, deploy_def) == [
        {'name': 'foo1', 'a': []}, {'name': 'foo2', 'a': []}]


def test_nodes_filter_not_enough():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []},
             {'name': 'foo3', 'a': []}]
    nodes_roles = {'foo1': ['f1', 'f2'], 'foo2': ['f2'], 'foo3': ['f3']}
    deploy_def = {'f1': 1, 'f2': 3, 'f3': 0}
    with pytest.raises(errors.AnsibleFilterError):
        nodes_filter(nodes, nodes_roles, deploy_def)


def test_role2nodes():
    nodes_roles = {'foo1': ['f1', 'f2'], 'foo2': ['f2'], 'foo3': ['f3']}
    assert role2nodes(nodes_roles) == {'f1': ['foo1'],
                                       'f2': ['foo1', 'foo2'],
                                       'f3': ['foo3']}


def test_nodes_name():
    nodes = [{'name': 'foo1', 'a': []},
             {'name': 'foo2', 'a': []},
             {'name': 'foo3', 'a': []}]
    assert nodes_name(nodes) == ['foo1', 'foo2', 'foo3']


def test_nodes_net_config():
    nodes = [{'name': 'foo1',
              'interfaces': [
                {'mac_address': 'macA1', 'address': 'ipA1', 'name': 'nic1'},
                {'mac_address': 'macA2', 'address': 'ipA2', 'name': 'nic2'}]}]
    net_config = {'net1': {'interface': 0}, 'net2': {'interface': 1}}
    assert nodes_net_config(nodes, net_config) == {
        'foo1': {'net1': {'ip': 'ipA1', 'mac': 'macA1'},
                 'net2': {'ip': 'ipA2', 'mac': 'macA2'}}}
