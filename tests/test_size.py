import size

"""
    Test Size module
"""

size_conv = size.FilterModule().filters()['size_conv']
size_sum = size.FilterModule().filters()['size_sum']


def test_size_conv_():
    assert size_conv('5') == 5


def test_size_conv_2Go():
    assert size_conv('5120', 'Go') == 5


def test_size_conv_Mo():
    assert size_conv('5Mo') == 5


def test_size_conv_Go():
    assert size_conv('5Go') == 5120


def test_size_conv_Go2To():
    assert size_conv('5Go', 'To') == 0


def test_size_conv_To():
    assert size_conv('5To') == 5242880


def test_size_conv_To2go():
    assert size_conv('5To', 'go') == 5120


def test_size_conv_To2Go():
    assert size_conv('5To', 'Go') == 5120


def test_size_sum_GoGo():
    assert size_sum(['5Go', '5Go']) == 10240


def test_size_sum_GoTo():
    assert size_sum(['5Go', '5To']) == 5248000


def test_size_sum_GoTo2Go():
    size_sum(['5Go', '5To'], 'Go') == 5125
