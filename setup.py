#!/usr/bin/env python3

from glob import glob
from os.path import basename
from os.path import splitext

from setuptools import find_packages
from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='infra_manager_filters',
      version='0.1',
      description='A set of filter_plugins for infra_manager',
      long_description=readme(),
      url='https://gitlab.com/Orange-OpenSource/lfn/infra/infra_manager_filter',
      author='Orange OpenSource',
      license='Apache 2.0',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      include_package_data=True,
      install_requires=[
      ],
      setup_requires=["pytest-runner"],
      tests_require=[
        "ansible",
        "pytest",
        "pytest-cov",],
      zip_safe=False)
