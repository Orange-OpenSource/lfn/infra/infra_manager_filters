#!/usr/bin/env python3
"""Size filter_plugins for Infra manager."""

import re


def size_conv(size_input, target_unit='Mo'):
    """
    Convert size into the targeted unit.

    :param size_input: octet size with Mo Go To unit
    :type size_input: string
    :param target_unit: the unit to convert to (default: Mo)
    :type target_unit: string
    :return: the converted size
    :rtype: string

    :Example:
    >>> size_conv('5Mo')
    5
    >>> size_conv('5Go')
    5120
    >>> size_conv('5Go', 'To')
    0
    >>> size_conv('5To')
    5242880
    >>> size_conv('5To', 'Go')
    5120
    """
    size_input = str(size_input)
    regex_p = re.compile(r'(\d+)+\s*([^\s]*)')
    (size, unit) = regex_p.findall(size_input)[0]
    size = int(size)
    unit = unit.lower()
    target_unit = target_unit.lower()
    if unit in ['g', 'go']:
        size *= 1024
    elif unit in ['t', 'to']:
        size *= 1024 * 1024

    if target_unit in ['g', 'go']:
        div = 1024
    elif target_unit in ['t', 'to']:
        div = 1024 * 1024
    else:
        div = 1
    return size // div


def size_sum(sizes, target_unit='Mo'):
    """
    Sum sizes, and convert to targeted unit.

    :param sizes: a list of sizes
    :type size: list
    :param target_unit: the unit to convert to (default: Mo)
    :type target_unit: string
    :return: the sum of sizes
    :rtype: string

    :Example:
    >>> size_sum(['5Go', '5Go'])
    10240
    >>> size_sum(['5Go', '5To'])
    5248000
    >>> size_sum(['5Go', '5To'], 'Mo')
    5248000
    """
    s_sum = 0
    for size in sizes:
        s_sum += size_conv(size, target_unit)
    return s_sum


class FilterModule:
    """Functions linked to node network interfaces."""

    @staticmethod
    def filters():
        """Ansible filters definition."""
        return {
            'size_conv': size_conv,
            'size_sum': size_sum
        }

# if __name__ == "__main__":
#     import doctest
#     doctest.testmod(raise_on_error=True)
