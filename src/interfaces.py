#!/usr/bin/env python3
"""Interface filter_plugins for Infra manager."""


def target_interfaces(inventory_hostname, hostvars, nodes):
    """
    Get the macs used on the node.

    :param inventory_hostname: the node name
    :type inventory_hostname: string
    :param hostvars: the ansible hostvars of the node
    :type hostvars: dict
    :param nodes: a dict of nodes
    :type nodes: dict
    :return: list of macs in (lower case) of interfaces plugged on the node
    :rtype: list

    :Example:
    >>> nodes = { 'foo':
    ...             { 'interfaces': [
    ...                 { 'mac_address': 'aa:bb:cc:dd:ee:f1' },
    ...                 { 'mac_address': 'aa:bb:cc:dd:ee:f2' }]
    ...             }
    ...         }
    >>> hostvars = { 'bar': { 'ansible_hostname': 'foo' }}
    >>> target_interfaces('bar', hostvars, nodes)
    ['aa:bb:cc:dd:ee:f1', 'aa:bb:cc:dd:ee:f2']
    """
    return [i['mac_address'].lower() for i in nodes[hostvars[
        inventory_hostname]['ansible_hostname']]['interfaces']]


def mac2intf(inventory_hostname, hostvars, nodes):
    """
    Get the node's macs associate to its interface name.

    :param inventory_hostname: the node name
    :type inventory_hostname: string
    :param hostvars: the ansible hostvars of the node
    :type hostvars: dict
    :param nodes: a dict of nodes
    :type nodes: dict
    :return: list of macs of interfaces plugged on the node, linked with
             the interfaces name
    :rtype: list

    :Example:
    >>> nodes = { 'foo':
    ...             { 'interfaces': [
    ...                 { 'mac_address': 'aa:bb:cc:dd:ee:f1' },
    ...                 { 'mac_address': 'aa:bb:cc:dd:ee:f2' },
    ...                 { 'mac_address': 'aa:bb:cc:dd:ee:f3' }]
    ...             }
    ...         }
    >>> hostvars = { 'bar':
    ...                 {
    ...                     'ansible_hostname': 'foo',
    ...                     'ansible_interfaces': [ 'eth0',
    ...                                             'eth1',
    ...                                             'eth1.123'],
    ...                     'ansible_eth0':
    ...                         { 'macaddress': 'aa:bb:cc:dd:ee:f1' },
    ...                     'ansible_eth1':
    ...                         { 'macaddress': 'aa:bb:cc:dd:ee:f2' },
    ...                     'ansible_eth1.123':
    ...                         { 'macaddress': 'aa:bb:cc:dd:ee:f2' }
    ...                 }
    ...             }
    >>> mac2intf('bar', hostvars, nodes)
    {'aa:bb:cc:dd:ee:f1': 'eth0', 'aa:bb:cc:dd:ee:f2': 'eth1'}
    """
    intf_list = hostvars[inventory_hostname]['ansible_interfaces']
    target_macs = target_interfaces(inventory_hostname, hostvars, nodes)
    macs = {}
    for intf in intf_list:
        # only recover physical interfaces
        if (intf.startswith('en') or intf.startswith('eth')) and \
                '.' not in intf:
            mac = hostvars[inventory_hostname]["ansible_{}".format(intf)
                                               ]['macaddress']
            if mac.lower() in target_macs:
                macs[mac] = intf
    return macs


class FilterModule:
    """Functions linked to node network interfaces."""

    @staticmethod
    def filters():
        """Ansible filters definition."""
        return {
            'target_interfaces': target_interfaces,
            'mac2intf': mac2intf,
        }

# if __name__ == "__main__":
#     import doctest
#     doctest.testmod(raise_on_error=True)
