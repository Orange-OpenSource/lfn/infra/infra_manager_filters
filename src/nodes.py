#!/usr/bin/env python3
"""Nodes filter_plugins for Infra manager."""

from ansible import errors


def nodes_as_dict(nodes):
    """
    Convert nodes list to dict indexed on nodes name.

    :param nodes: the nodes list
    :type nodes: list
    :return: dictionnary of nodes
    :rtype: dict

    :Example:
    >>> nodes = [
    ...           { 'name': 'foo1', 'a': [] },
    ...           { 'name': 'foo2', 'a': [] }
    ...         ]
    >>> nodes_as_dict(nodes)
    {'foo1': {'name': 'foo1', 'a': []}, 'foo2': {'name': 'foo2', 'a': []}}
    """
    return {n['name']: n for n in nodes}


def nodes_index(nodes, zero_key='jumphost0'):
    """
    Add an index to node list.

    :param nodes: the nodes list
    :type nodes: list
    :return: dictionnary of nodes, indexed
    :rtype: dict

    :Example:
    >>> nodes = [
    ...           { 'name': 'foo1', 'a': [] },
    ...           { 'name': 'foo2', 'a': [] }
    ...         ]
    >>> nodes_index(nodes)
    {'foo1': 1, 'foo2': 2}
    """
    n_index = {}
    inc = 1
    for index, name in enumerate(sorted([n['name'] for n in nodes])):
        if name == zero_key:
            n_index[name] = 0
            inc = 0
        else:
            n_index[name] = index+inc
    return n_index


def nodes_filter(nodes, nodes_roles, deploy_def):
    """
    Filter a list of node to fit the deploy_size needs.

    :param nodes: the nodes list
    :type nodes: list
    :param nodes_roles: roles mapping from idf file
    :type nodes_roles: dict
    :param deploy_def: roles distribution (deploy_definitions[deploy_size])
    :type deploy_def: dict
    :return: list of nodes, filtered
    :rtype: list

    :Example:
    >>> nodes = [
    ...           { 'name': 'foo1', 'a': [] },
    ...           { 'name': 'foo2', 'a': [] },
    ...           { 'name': 'foo3', 'a': [] }
    ...         ]
    >>> roles = {'foo1': ['f1', 'f2'], 'foo2': ['f2'], 'foo3': ['f3']}
    >>> deploy_def = {'f1': 1, 'f2': 2, 'f3': 0}
    >>> nodes_filter(nodes, roles, deploy_def)
    [{'name': 'foo1', 'a': []}, {'name': 'foo2', 'a': []}]
    """
    filtered_list = []
    nodes_d = nodes_as_dict(nodes)
    roles_map = role2nodes(nodes_roles)
    for role, qty in deploy_def.items():
        if qty > len(roles_map[role]):
            print('not enought node for role {}'.format(role))
            raise errors.AnsibleFilterError(
                'not enought node for role {}'.format(role))
        for i in range(0, qty):
            node_name = roles_map[role][i]
            filtered_list.append(nodes_d[node_name])
    # filter duplicate
    return [nodes_d[node] for node in
            sorted(set([n['name'] for n in filtered_list]))]


def role2nodes(nodes_roles):
    """
    Get a dictionnary containing nodes associate to a role.

    :param nodes_roles: roles mapping from idf file
    :type nodes_roles: dict
    :return: roles mapped to nodes name
    :rtype: dict

    :Example:
    >>> roles = {'foo1': ['f1', 'f2'], 'foo2': ['f2'], 'foo3': ['f3']}
    >>> role2nodes(roles)
    {'f1': ['foo1'], 'f2': ['foo1', 'foo2'], 'f3': ['foo3']}
    """
    roles = {}
    for node in sorted(nodes_roles):
        for role in nodes_roles[node]:
            if role not in roles.keys():
                roles[role] = []
            roles[role].append(node)
    return roles


def nodes_name(nodes):
    """
    Return a list of nodes name.

    :param nodes: the nodes list
    :type nodes: list
    :return: list of nodes names
    :rtype: list

    :Example:
    >>> nodes = [
    ...           { 'name': 'foo1', 'a': [] },
    ...           { 'name': 'foo2', 'a': [] },
    ...           { 'name': 'foo3', 'a': [] }
    ...         ]
    >>> nodes_name(nodes)
    ['foo1', 'foo2', 'foo3']
    """
    return sorted(node['name'] for node in nodes)


def nodes_net_config(nodes, net_config):
    """
    Get a dictionnary containing the ip of each network of all nodes.

    :param nodes: the nodes list
    :type nodes: list
    :param net_config: net_config from IDF
    :type net_config: dict
    :return: list of nodes names
    :rtype: list

    :Example:
    >>> nodes = [
    ...   { 'name': 'foo1', 'interfaces': [
    ...     {'mac_address': 'macA1', 'address': 'ipA1', 'name': 'nic1'},
    ...     {'mac_address': 'macA2', 'address': 'ipA2', 'name': 'nic2'}]}]
    >>> net_config = {
    ...     'net1': {'interface': 0},
    ...     'net2': {'interface': 1},
    ... }
    >>> nodes_net_config(nodes, net_config)
    ... # doctest: +NORMALIZE_WHITESPACE
    {'foo1': {'net1': {'ip': 'ipA1', 'mac': 'macA1'},
              'net2': {'ip': 'ipA2', 'mac': 'macA2'}}}
    """
    n_net_config = {}
    for node in nodes:
        n_net_config[node['name']] = {}
        nic_map = {
            intf['name']: {
                'ip': intf['address'] if 'address' in intf.keys() else '',
                'mac': intf['mac_address']
            }
            for intf in node['interfaces']
        }
        for net_name, net_cfg in net_config.items():
            if 'nic{}'.format(net_cfg['interface']+1) in nic_map.keys():
                n_net_config[node['name']][net_name] = \
                    nic_map['nic{}'.format(int(net_cfg['interface'])+1)]
    return n_net_config


class FilterModule:
    """Functions linked to node network interfaces."""

    @staticmethod
    def filters():
        """Ansible filters definition."""
        return {
            'role2nodes': role2nodes,
            'nodes_as_dict': nodes_as_dict,
            'nodes_filter': nodes_filter,
            'nodes_index': nodes_index,
            'nodes_name': nodes_name,
            'nodes_net_config': nodes_net_config,
        }

# if __name__ == "__main__":
#     import doctest
#     doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE,
#                     raise_on_error=True)
